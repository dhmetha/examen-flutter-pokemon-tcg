import 'package:tp_3/models/attack_model.dart';
import 'package:tp_3/models/weakness_model.dart';

class PokeTcgCard {
  final String id;
  final String name;
  final int nationalPokedexNumber;
  final String imageUrl;
  final String imageUrlHiRes;
  final List<String> types;
  final String supertype;
  final String subtype;
  final String evolvesfrom;
  final String hp;
  final List<String> retreatCost;
  final String number;
  final String artist;
  final String rarity;
  final String series;
  final String set;
  final String setCode;
  final List<Attack> attacks;
  final List<Weakness> weaknesses;

  PokeTcgCard(
      {this.id,
      this.name,
      this.nationalPokedexNumber,
      this.imageUrl,
      this.imageUrlHiRes,
      this.types,
      this.supertype,
      this.subtype,
      this.evolvesfrom,
      this.hp,
      this.retreatCost,
      this.number,
      this.artist,
      this.rarity,
      this.series,
      this.set,
      this.setCode,
      this.attacks,
      this.weaknesses});

  factory PokeTcgCard.fromJson(Map<String, dynamic> json) {
    return PokeTcgCard(
      id: json['id'],
      name: json['name'],
      nationalPokedexNumber: json['nationalPokedexNumber'],
      imageUrl: json['imageUrl'],
      imageUrlHiRes: json['imageUrlHiRes'],
      types: (json['types'] == null) ? List<String>() : List<String>.from(json['types']),
      supertype: json['supertype'],
      subtype: json['subtype'],
      evolvesfrom: json['evolvesfrom'],
      hp: json['hp'],
      retreatCost: (json['retreatCost'] == null) ? List<String>() : List<String>.from(json['retreatCost']),
      number: json['number'],
      artist: json['artist'],
      rarity: json['rarity'],
      series: json['series'],
      set: json['set'],
      setCode: json['setCode'],
      attacks: List<Attack>(),
      weaknesses: List<Weakness>(),
    );
  }
}
