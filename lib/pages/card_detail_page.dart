import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tp_3/models/card_model.dart';
import 'package:tp_3/widgets/scaffold_widget.dart';

class CardDetailPage extends StatelessWidget {
  final PokeTcgCard pokeTcgCard;

  CardDetailPage({Key key, @required this.pokeTcgCard}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(pokeTcgCard);
    return customScaffoldWidget(
      context: context,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Center(
                child: CachedNetworkImage(
                  imageUrl: pokeTcgCard.imageUrlHiRes,
                  placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) =>
                      Icon(Icons.error, color: Colors.red),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    _buildCustomPropertyRow(title: 'Id', value: pokeTcgCard.id),
                    _buildCustomPropertyRow(
                        title: 'Name', value: pokeTcgCard.name),
                    _buildCustomPropertyRow(
                        title: 'National Pokedex Number',
                        value: pokeTcgCard.nationalPokedexNumber.toString()),
                    _buildCustomTypesRow(),
                    _buildCustomPropertyRow(
                        title: 'Super Type', value: pokeTcgCard.supertype),
                    _buildCustomPropertyRow(
                        title: 'Subtype', value: pokeTcgCard.subtype),
                    _buildCustomPropertyRow(
                        title: 'Evolves From', value: pokeTcgCard.evolvesfrom),
                    _buildCustomPropertyRow(title: 'Hp', value: pokeTcgCard.hp),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCustomSpacer() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(height: 1, color: ThemeData.dark().accentColor),
    );
  }

  Widget _buildCustomTypesRow() {
    List<Text> types = List<Text>();

    for (String type in pokeTcgCard.types) {
      types.add(Text(type));
    }

    return Column(
      children: <Widget>[
        _buildCustomSpacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text('Types: ', style: TextStyle(fontWeight: FontWeight.bold)),
            Row(children: types),
          ],
        ),
      ],
    );
  }

  Widget _buildCustomPropertyRow(
      {@required String title, @required String value}) {
    if (value == null) return Container();
    return Column(
      children: <Widget>[
        _buildCustomSpacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              '$title:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(value),
          ],
        ),
      ],
    );
  }
}
