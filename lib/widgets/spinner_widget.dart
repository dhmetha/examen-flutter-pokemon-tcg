import 'package:flutter/material.dart';

Widget customSpinnerWidget({double value}) {
  return Center(
    child: Padding(
      padding: EdgeInsets.all(8.0),
      child: SizedBox(
        width: 128,
        height: 128,
        child: CircularProgressIndicator(strokeWidth: 8.0),
      ),
    ),
  );
}
