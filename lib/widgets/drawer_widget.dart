import 'package:flutter/material.dart';
import 'package:tp_3/page_route.dart';
import 'package:tp_3/pages/cards_page.dart';
import 'package:tp_3/pages/home_page.dart';

Widget customDrawerWidget(BuildContext context) {
  return Drawer(
    child: ListView(
      // Important: Remove any padding from the ListView.
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: FittedBox(
            fit: BoxFit.contain,
            child: Image(image: AssetImage('assets/pokemon_tcg_logo.png')),
          ),
        ),
        ListTile(
          title: Text('Home'),
          onTap: () => Navigator.push(
            context,
            CustomPageRoute(builder: (context) => new HomePage()),
          ),
        ),
        ListTile(
          title: Text('Cards'),
          onTap: () => Navigator.push(
            context,
            CustomPageRoute(builder: (context) => new CardsPage()),
          ),
        ),
      ],
    ),
  );
}
